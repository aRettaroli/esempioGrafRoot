#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include "TApplication.h"
#include "TH2.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraph.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TRint.h"
using namespace std;

int NumberOfLines(string file_path);


int main() {
	
	string path = "dataQ0_9GHz_zfc.dat";
	int ii = NumberOfLines(path);

	string pathFC = "dataQ0_9GHz_fc.dat";
	int ii_fc = NumberOfLines(pathFC);
	
	
	double q[ii];
	double b[ii];
	double err_q[ii];

	double q_fc[ii_fc];
	double b_fc[ii_fc];
	double err_q_fc[ii_fc];
	

	//--------------------READ ZFC FILE----------------------------
	ifstream file;
	file.open(path);
	
	string buf1,buf2,buf3,buf4;
	int counter = 0;
	
	while( file>>buf1 ){
		
		file>>buf2;
		file>>buf3;
		file>>buf4;
		
		if(buf1[0]=='#' || buf1[0]=='\0' || buf1[0]=='\r' || buf1[0]=='\n') continue;
		
		else {
			
			b[counter] = stod(buf1,NULL);
			q[counter] = stod(buf3,NULL);
			err_q[counter] = stod(buf4,NULL);
			counter++;
		}
	}
	file.close();
	//-------------------------------------------------------------
	
	//--------------------READ FC FILE----------------------------
	ifstream fileFC;
	fileFC.open(pathFC);
	
	// reset counter
	counter = 0;
	
	while( fileFC>>buf1 ){
		
		fileFC>>buf2;
		fileFC>>buf3;
		fileFC>>buf4;
		
		if(buf1[0]=='#' || buf1[0]=='\0' || buf1[0]=='\r' || buf1[0]=='\n') continue;
		
		else {
			
			b_fc[counter] = stod(buf1,NULL);
			q_fc[counter] = stod(buf3,NULL);
			err_q_fc[counter] = stod(buf4,NULL);
			counter++;
		}
	}
	fileFC.close();
	//-------------------------------------------------------------
	
	
	
	
	///////////////////// GRAPHS /////////////////////////////////////////
	
	TApplication myApp("myApp",0,0);
	
	TGraphErrors *runB_zfc = new TGraphErrors(ii,b,q,0,err_q);
	runB_zfc->SetName("runB_9GHz_zfc");

	TGraphErrors *runB_fc = new TGraphErrors(ii_fc,b_fc,q_fc,0,err_q_fc);
	runB_fc->SetName("runB_9GHz_fc");

	TF1 *cu = new TF1("copper","90642.",-1,6);
	TGraph *cop = new TGraph(cu);
	cop->SetName("copperQ");
	cop->SetLineColor(kAzure+7);
	cop->SetLineWidth(3);

	TCanvas *c = new TCanvas("c","c",700,700);
	//c5->SetGrid();

	runB_zfc->SetLineColor(1);
//	runB_zfc->SetLineWidth(0);
	runB_zfc->SetTitle("");
	runB_zfc->GetXaxis()->SetTitle("B   [T]");
	runB_zfc->GetYaxis()->SetTitle("Q_{0}");
	runB_zfc->SetMarkerStyle(21);
	runB_zfc->SetMarkerColor(4);
	runB_zfc->SetMarkerSize(0.5);

	runB_fc->SetLineColor(1);
//	runB_fc->SetLineWidth(0);
	runB_fc->SetTitle("");
	runB_fc->GetXaxis()->SetTitle("B   [T]");
	runB_fc->GetYaxis()->SetTitle("Q_{0}");
	runB_fc->SetMarkerStyle(20);
	runB_fc->SetMarkerColor(2);
	runB_fc->SetMarkerSize(0.5);

	TMultiGraph *mg = new TMultiGraph();
	mg->Add(runB_zfc);
	mg->Add(runB_fc);
	mg->SetTitle(" ;B   [T];Q_{0}");
	//mg->GetXaxis()->SetTitle("B  [T]");
	//mg->GetYaxis()->SetTitle("Q_{0}");
	mg->GetXaxis()->SetTitleSize(0.055);
	mg->GetYaxis()->SetTitleSize(0.055);
	mg->GetXaxis()->SetLabelSize(0.045);
	mg->GetYaxis()->SetLabelSize(0.045);
	//mg->GetXaxis()->SetTitleOffset(0.95);
	mg->GetYaxis()->SetTitleOffset(1.25);
	mg->GetXaxis()->CenterTitle();
	mg->GetYaxis()->CenterTitle();
	mg->GetXaxis()->SetLimits(0.,5.6);
	mg->GetYaxis()->SetRangeUser(0.,1300000.);
	mg->Draw("ap");
	cop->Draw("same");

	TLegend *leg = new TLegend(1,2,1,2);
	leg->AddEntry("runB_9GHz_zfc","ZFC data","LP");
	leg->AddEntry("runB_9GHz_fc","FC data","LP");
	leg->AddEntry("copperQ","Copper","L");
	leg->SetFillStyle(0);
	leg->SetLineWidth(1);
	leg->Draw();
	gStyle->SetLegendTextSize(0.033);

// FOR MORE CUSTOMIZATION SEE TAxis() attributes in the ROOT Class List, and TAttAxis() attributes.
	
	////////////////////////////////////////////////////////////

	myApp.Run(kTRUE);
	
	return 1;
}

//------------------------------------------------------

int NumberOfLines(string file_path) {
//	reads the number of lines that have data. Ignores comments and blank lines.
	
	ifstream file;
	file.open(file_path);
	
	string lineContent;
	
	int lineNumber=0;
	
	if( file.is_open() ) {
		
		while( getline(file,lineContent) ) {
			
			if (lineContent[0] == '#' || lineContent[0] == '\r' || lineContent[0] == '\n') continue;
			else lineNumber++;
		}
		
	} else cerr << "Error in opening " << file_path << endl;
	
	
	file.close();
	
	return lineNumber;
}

